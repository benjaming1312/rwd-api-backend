import Vue from 'vue'
import Router from 'vue-router'
import OnlineCheck from '@/components/OnlineCheck'
import admin from '@/components/Admin'

// 使用iview
import BootstrapVue from 'bootstrap-vue'

Vue.use(Router)
Vue.use(BootstrapVue)

export default new Router({
  // mode: 'history',
  routes: [
    // 路徑
    {
      path: '/',
      name: 'onlinecheck',
      component: OnlineCheck
    },
    // 路徑
    {
      path: '/admin',
      name: 'admin',
      component: admin
    },
  ]
})
