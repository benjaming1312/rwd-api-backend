// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

// 使用iview
import iView from 'iview'
import 'iview/dist/styles/iview.css'

// 引入
Vue.use(iView)
Vue.config.productionTip = false

/* eslint-disable no-new */

// 介面
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: {App}
})

